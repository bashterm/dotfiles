CUR_DIR="$(pwd)"

cd ~/
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

cp ${CUR_DIR}/.bashrc ~/.bashrc
cp ${CUR_DIR}/.zshrc ~/.zshrc
cp ${CUR_DIR}/.Xdefaults ~/.Xdefaults
